## Django XML Information Serve

GSoC selection test for Project: [Ingesting the Canadian Common CV](https://bitbucket.org/mugqic/gsoc_2020/src/master/#markdown-header-ingesting-the-canadian-common-cv)

> by Chinmay Vibhute

### Deployment

    > pipenv install
    > pipenv shell
    > python manage.py runserver

\*\*\* Install pipenv if not installed, using pip install pipenv

or

    Using Docker
    > docker build -t chinvib66/g3c-test .
    > docker run -it -p 8000:8000 chinvib66/g3c-test

Open [http://localhost:8000](http://localhost:8000) in browser

### Endpoints

- /

        Lists all entries from the database.
        Default order is ascending order by CreationDate

- /?ordering=**\_\_**

        Values Supported: ViewCount, Score, CreationDate.
        Default ascending Order; use '-' for descending order eg: ?ordering=-Score
        Follow DRF Documentation (https://www.django-rest-framework.org/api-guide/filtering/#orderingfilter)

- /?string=**\_\_**

        Supported Fields: Body, Title.
        Follow DRF Documentation (https://www.django-rest-framework.org/api-guide/filtering/#searchfilter)

- /parse

        Stores (or updates) 'files/bioinformatics_posts_se.xml' data in sqlite3 database
        If xml file contains element with attribute which is not defined in model, the element will be skipped. Id of skipped elements will be displayed
        Note: This step has already been executed. Execute this if only the xml file is changed.
