from rest_framework import serializers

from .models import BPost


class BPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = BPost
        fields = '__all__'
