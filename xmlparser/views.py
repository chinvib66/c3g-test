import xml.etree.ElementTree as ET
from django.shortcuts import render
from django.http import JsonResponse
from django.db.models import Q
from rest_framework import generics, response, filters

# Create your views here.
from .models import BPost
from .serializers import BPostSerializer


class toDB(generics.GenericAPIView):

    def get(self, request):
        tree = ET.parse('./files/bioinformatics_posts_se.xml')
        c = 0
        errors = []
        for element in tree.getiterator('row'):
            try:
                post = BPost(**element.attrib)
                post.save()
                c += 1
            except Exception as e:
                errors.append({'ID': element.attrib['Id'], 'error': str(e)})
        return response.Response({'count': c, 'errors': errors})


class FilterData(generics.ListAPIView):
    queryset = BPost.objects.all()
    serializer_class = BPostSerializer
    filter_backends = [filters.OrderingFilter, filters.SearchFilter]
    ordering_fields = ['ViewCount', 'Score', 'CreationDate']
    search_fields = ['Body', 'Title']
