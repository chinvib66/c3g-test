from django.db import models

# Create your models here.


class BPost(models.Model):
    Id = models.IntegerField(blank=True, primary_key=True)
    OwnerUserId = models.IntegerField(blank=True)
    PostTypeId = models.IntegerField(blank=True)
    ParentId = models.IntegerField(blank=True, null=True)
    OwnerDisplayName = models.CharField(
        blank=True, null=True, default=None, max_length=255)
    Title = models.TextField(blank=True)
    Body = models.TextField(blank=True)
    Score = models.IntegerField(blank=True, default=0)
    Tags = models.TextField(blank=True)
    CreationDate = models.DateTimeField(blank=True)
    ClosedDate = models.DateTimeField(blank=True, null=True, default=None)
    ViewCount = models.IntegerField(blank=True, default=0)
    AnswerCount = models.IntegerField(blank=True, default=0)
    CommentCount = models.IntegerField(blank=True, default=0)
    AcceptedAnswerId = models.IntegerField(blank=True, null=True, default=0)
    FavoriteCount = models.IntegerField(blank=True, null=True, default=0)
    LastActivityDate = models.DateTimeField(blank=True, null=True)
    LastEditorUserId = models.IntegerField(blank=True, null=True)
    LastEditDate = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ['CreationDate']
